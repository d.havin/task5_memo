import { EDIT_USER } from "../constants/user.constants";

export const editUser = (data) => ({type: EDIT_USER, data});