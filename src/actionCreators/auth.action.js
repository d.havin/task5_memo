import { LOGIN, LOGOUT } from "../constants/auth.constants";

export const login = (data) => ({type: LOGIN, data});
export const logout = (data) => ({type: LOGOUT, data});

