import {EDIT_USER} from '../constants/user.constants'

export const defaultUserState = 
    {
    id: 1,
    name: 'Ivan',
    surName: 'Gavrikov',
    cartInfo: "3213 1231 4534 2342"
    };

export const profileReducer = (state = defaultUserState, action) => {
    switch (action.type) {
        case EDIT_USER: 
            let newState = {
                name: action.data.name,
                surName: action.data.surName,
                cartInfo: action.data.cartInfo
            };
            return newState;
        default:
            return state;
    }
};  
