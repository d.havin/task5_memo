import React, {useContext, useState} from 'react';
import '../styles/userPage.css';
import Context from '../utils/context';

function ProductInfo(props) {
    const [inputValue, setInputValue] = useState('');
    const context = useContext(Context)

    const handleSearch = () => {
    context.searchProduct(inputValue)
    }

    return (
        <>
        <div className = "searchMenu">
            <h2 className="findProduct">Найти товар</h2>
            Введите название продукта:<input value={inputValue} type="text" placeholder="Название продукта" onChange={(e) => setInputValue(e.target.value)}/>
            <button onClick={handleSearch}>Найти</button>
        </div>
        {
            context.productState.map(object => (
                <div className = "generalProductBox" key={object.id}>
                    <div className = "staff">
                        <p className ="productName">{object.name} </p>
                        <img src={object.img } alt = {object.name}></img>
                    </div>
                    <div className = "description">
                        <p className ="descriptionTitle">{object.name} </p>
                        <div>{object.description}</div>
                        <p>Цена: {object.price}</p>
                    </div>
                </div>
            ))
        }
        </>
    ) 
}

export default ProductInfo;
