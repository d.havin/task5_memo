import React, {useState, useContext} from 'react';
import '../styles/profilePage.css';
import Context from '../utils/context';
import ProfileInput from './unput.component';

function ProfileInfo(props) {
    const [editedName, setName] = useState('');
    const [editedSurName, setSurName] = useState('');
    const [editedCartInfo, setCartInfo] = useState('');
    const context = useContext(Context)
    
    const handleEdit = () => {
        let user = {
            name : editedName ? editedName: context.userState.name,
            surName: editedSurName ? editedSurName: context.userState.surName,
            cartInfo: editedCartInfo ? editedCartInfo : context.userState.cartInfo,
           
        }
        context.editUser(user)
        // console.log(editedCartInfo.length)
        setName('');
        setSurName('');
        setCartInfo('');
    }

    // const checkAuthState = () => {


    // }

    return (
        <div className = "editInfoMenu">
            <h2 className="editUserTitle">Изменить данные</h2>
            <div className="userInfo"> Данные пользователя: {context.userState.name} {context.userState.surName} {context.userState.cartInfo}</div>
            Имя: <ProfileInput data = {editedName} setInputData = {setName}  />
            Фамилия: <ProfileInput data = {editedSurName} setInputData = {setSurName}/>
            Номер карты: <ProfileInput data = {editedCartInfo} setInputData = {setCartInfo} flag = {true}/>
            <button onClick={handleEdit}>Изменить</button>
            {/* <button onClick={checkAuthState}></button> */}
        </div>
    )
}

export default ProfileInfo
