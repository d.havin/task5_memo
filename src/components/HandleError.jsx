import React from 'react';

const  HandleError = (props) => {

  const alertMessage = ()=> {
      if ( props.length > 1  && props.length < 16  ) {
       return 'Недостаточное количество символов' 
      } else if ( props.length === 16 ) { 
        return 'Отлично, теперь можем сохранить'
      } else if ( props.length > 16 )
        return 'Стоп, это уже перебор. Вы ввели слишком много символов'
  }
  console.log(props.length) // вывод длинны номера

  return (
    <div>
      <h2>{alertMessage()}</h2>
    </div>
  )
}

export default React.memo(HandleError, (prevProps, nextProps)=>{
  if (nextProps.length >= 15 && nextProps.length <= 16) {
    return false
  } else if (prevProps.length >= 15 && prevProps.length <= 16) {
    return false
  } return true
})