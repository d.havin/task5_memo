import React from "react";
import { NavLink } from "react-router-dom";
import "../styles/navbar.css"

function Navbar() {
  return (

    <nav className="overNav">
      <div className = "linkButton">
        <NavLink to="/adminPage">Admin Part</NavLink>
      </div>
      <div className = "linkButton">
        <NavLink to="/userPage">User Part</NavLink>
      </div>
      <div className = "linkButton">
        <NavLink to="/userProfile">Profile</NavLink>
      </div>
    </nav>
    
  );
}

export default Navbar;
